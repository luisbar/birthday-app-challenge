import React from 'react'
import HelmetProvider from 'react-navi-helmet-async'
import SpinnerProvider from '@atoms/GlobalSpinner'
import AlertProvider from '@molecules/GlobalAlert'
import InternationalizationProvider from '@internationalization/InternationalizationProvider'
import { render as originalRender } from '@testing-library/react'

const render = (ui, { locale = 'es', ...renderOptions } = {}) => {
  
  const Wrapper = ({ children }) => {
    return (
      <InternationalizationProvider>
          <AlertProvider>
            <SpinnerProvider>
              <HelmetProvider>
                {children}
              </HelmetProvider>
            </SpinnerProvider>
          </AlertProvider>
      </InternationalizationProvider>
    )
  }

  return originalRender(ui, { wrapper: Wrapper, ...renderOptions })
}

export * from '@testing-library/react'

export { render }
