import * as languages from '@internationalization/languages';

export const getLanguage = (languageSelected) => {
  const languagesEnables = Object.keys(languages);
  const language = languageSelected || navigator.language.split(
    /[-_]/,
  )[0];

  return languagesEnables.includes(
    language,
  ) ? language : languagesEnables[0];
};

export const getMessage = (languageSelected) => {
  const messages = {};
  Object
    .entries(languages)
    .forEach((language) => {
      const [ key, value ] = language;
      messages[key] = value;
    });

  return messages[getLanguage(languageSelected)];
};
