import { route, redirect, map } from 'navi';
import privatePages from '@routes/config/privatePages.json';
import upperCamelCaseToLowerCamelCase from '@utils/upperCamelCaseToLowerCamelCase';
import isAuthenticated from '@routes/guards/isAuthenticated';

export default async () => {
  const routes = {};

  const getConfiguration = async (privatePage) => {
    try {
      return await import(
        `@pages/private/config/${upperCamelCaseToLowerCamelCase(
          privatePage.name,
        )}`
      );
    } catch (error) {
      return {
        default: {},
      };
    }
  };

  await Promise.all(
    privatePages.map((privatePage) => new Promise(async (resolve, reject) => {
      try {
        const configuration = await getConfiguration(privatePage);
        const path = configuration.default.path || privatePage.path;

        routes[path] = map(
          () => {
            if (isAuthenticated()) return route(
              {
                title: privatePage.name,
                getView: () => import(
                  /* webpackChunkName: 'private' */ `@pages/private/${privatePage.name}.${
                    privatePage.extension
                  }`
                ),
                ...configuration.default,
              },
            );

            return redirect(
              '/login',
            );
          },
        );

        resolve();
      } catch (error) {
        reject(error);
      }
    })),
  );

  return { ...routes };
};
