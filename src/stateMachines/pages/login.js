import { createMachine } from 'xstate';
import loginFormMachine from '@stateMachines/organisms/loginForm';
import { assign } from '@xstate/immer';

export const machineDefinition = {
  id: 'login',
  initial: 'idle',
  context: {
    user: undefined,
  },
  invoke: [
    {
      id: 'loginForm',
      src: loginFormMachine,
    },
  ],
  states: {
    idle: {
      on: {
        SAVE_SESSION: {
          actions: [ 'saveSession' ],
          target: 'idle',
        },
      },
    },
  },
};

const machineOptions = {
  actions: {
    saveSession: assign((context, event) => { context.user = event.data; }),
    saveError: assign((context, event) => { context.error = event.data; }),
  },
};

export default createMachine(machineDefinition, machineOptions);