import { assign } from '@xstate/immer';
import { createMachine, actions, send } from 'xstate';
import checkboxMachine from '@stateMachines/atoms/checkbox';
import dropdownMachine from '@stateMachines/atoms/dropdown';
import inputMachine from '@stateMachines/atoms/input';
import logOutFormMachine from '@stateMachines/organisms/logOutForm';
import requester from '@root/utils/requester';
import paginatorMachine from '@stateMachines/molecules/paginator';

const INPUTS_CHILDREN = [ 'rateNameInput' ];
const CHILDREN_TO_DISABLED = [ ...INPUTS_CHILDREN ];
const DEFAULT_PAGE_NUMBER = 1;
const DEFAULT_PAGE_SIZE = 10;

export const machineDefinition = {
  id: 'home',
  initial: 'idle',
  context: {
    rates: [],
    ratesFiltered: [],
    pagination: {
      pageNumber: DEFAULT_PAGE_NUMBER,
      pageSize: DEFAULT_PAGE_SIZE,
    },
  },
  invoke: [
    {
      id: 'changeThemeCheckbox',
      src: checkboxMachine,
    },
    {
      id: 'logOutForm',
      src: logOutFormMachine,
    },
    {
      id: 'languagesDropdown',
      src: dropdownMachine,
    },
    {
      id: 'rateNameInput',
      src: inputMachine,
    },
    {
      id: 'paginator',
      src: paginatorMachine,
    },
  ],
  states: {
    idle: {
      on: {
        PROCESS: [
          {
            target: 'processing',
          },
        ],
        CHANGE_THEME: [
          {
            actions: [ 'changeTheme' ],
            target: 'processing',
          },
        ],
      },
    },
    processing: {
      entry: [ 'disableAll', 'updatePageNumber' ],
      invoke: [
        {
          id: 'fetchRates',
          src: 'fetchRates',
          onDone: {
            actions: [ 'saveInContext' ],
            target: 'finished',
          },
          onError: {
            actions: [ 'saveError' ],
            target: 'failed',
          },
        },
      ],
      exit: [ 'enableAll', 'updatePaginationOnPaginator' ],
    },
    finished: {
      on: {
        FILTER: [
          {
            actions: [ 'filterRates' ],
            target: 'finished',
          },
        ],
        CHANGE_THEME: [
          {
            actions: [ 'changeTheme' ],
            target: 'finished',
          },
        ],
        PROCESS: [
          {
            target: 'processing',
          },
        ],
      },
    },
    failed: {
      after: {
        1: {
          target: 'idle',
        },
      },
    },
  },
};

const machineOptions = {
  actions: {
    disableAll: actions.pure(() => CHILDREN_TO_DISABLED.map((child) => send({ type: 'DISABLE' }, { to: child, delay: 1 }))),
    enableAll: actions.pure(() => CHILDREN_TO_DISABLED.map((child) => send({ type: 'ENABLE' }, { to: child }))),
    changeTheme: () => document.documentElement.classList.toggle('dark'),
    saveError: assign((context, event) => { context.error = event.data; }),
    saveInContext: assign((context, event) => {
      context.rates = event.data;
    }),
    updatePageNumber: assign((context, { data: { pageNumber } = {} }) => {
      context.pagination.pageNumber = pageNumber;
    }),
    updatePaginationOnPaginator: send(({ pagination }) => ({ type: 'UPDATE_PAGINATION', data: { pagination } }), { to: 'paginator' }),
    filterRates: assign((context, { data: { filter } = {} }) => {
      if (filter) context.ratesFiltered = context.rates
        .filter((rate) => rate.name.toLowerCase().includes(filter.toLowerCase()));
      else context.ratesFiltered = [];
    }),
  },
  services: {
    fetchRates: async ({ pagination: { pageNumber, pageSize } }) => {
      const rates = await requester.get(`/assets?limit=${pageSize}&offset=${pageNumber * pageSize - pageSize}`);
      return rates.data.data
        .map((rate) => ({
          ...rate,
          priceUsd: Number(rate.priceUsd).toFixed(2),
          changePercent24Hr: Number(rate.changePercent24Hr).toFixed(2),
        }));
    },
  },
};

export default createMachine(machineDefinition, machineOptions);