import propTypes from 'prop-types';
import React from 'react';
import SideMenu from '@organisms/SideMenu';
import Checkbox from '@atoms/Checkbox';
import Dropdown from '@atoms/Dropdown';
import { useStoreContext } from '@root/stateMachines/Store';
import { useInternationalizationContext } from '@root/internationalization/InternationalizationProvider';

const Main = ({ children }) => {
  const { homeService } = useStoreContext();
  const setLanguage = [ ...useInternationalizationContext() ].pop();

  const onChangeLanguage = ({ target }) => {
    console.log(target.value);
    setLanguage(target.value);
  };

  return (
    <div
      className='w-full h-full flex flex-row dark:bg-dark-100'
    >
      <SideMenu
        items={[
          {
            path: '/',
            value: 'sideMenu.txt1',
          },
          {
            path: '/components',
            value: 'sideMenu.txt2',
          },
        ]}
      />
      <div
        className='w-full grid grid-rows-main grid-cols-10 gap-y-5 gap-x-5 p-5'
      >
        <Dropdown
          className='row-start-1 col-start-9 justify-self-end self-center'
          machine={homeService.children.get('languagesDropdown')}
          select={{
            options: [
              {
                key: 'en',
                value: 'main.txt1',
              },
              {
                key: 'es',
                value: 'main.txt2',
              },
            ],
            onChange: onChangeLanguage,
          }}
          text={{
            label: 'main.txt3',
          }}
        />
        <Checkbox
          label='main.txt4'
          className='row-start-1 col-start-10 justify-self-end self-center'
          data={{
            type: 'CHANGE_THEME',
          }}
          machine={homeService.children.get('changeThemeCheckbox')}
        />
        {children}
      </div>
    </div>
  );
};

Main.propTypes = {
  children: propTypes.node.isRequired,
};

export default Main;