import React from 'react';
import * as icons from 'react-icons/bs';
import Text from '@atoms/Text';
import propTypes from 'prop-types';

const getValueUsingDotNotation = ({ columnKey, row }) => columnKey.split('.').reduce((accumulator, item) => accumulator[item], row);

const onClickAction = ({ row, onClickItem }) => () => {
  onClickItem(row);
};

const onClickHeaderAction = ({ onClickItem }) => () => {
  onClickItem();
};

const Cell = {
  header: ({ columnIndex, label }) => (
    <Text
      className={`row-start-2 col-start-${columnIndex + 1} self-center justify-self-center`}
      label={label}
      as='h6'
    />
  ),
  change: ({ rowIndex, columnIndex, row, columnKey }) => {
    const value = getValueUsingDotNotation({ columnKey, row });
    return (
      <Text
        className={`row-start-${rowIndex + 1} col-start-${columnIndex + 1} self-center justify-self-center ${value > 1 ? 'text-red-500 dark:text-red-200' : 'text-green-500 dark:text-green-200'}`}
        label={getValueUsingDotNotation({ columnKey, row })}
      />
    );
  },
  text: ({ rowIndex, columnIndex, row, columnKey }) => (
    <Text
      className={`row-start-${rowIndex + 1} col-start-${columnIndex + 1} self-center justify-self-center`}
      label={getValueUsingDotNotation({ columnKey, row })}
    />
  ),
  actions: ({ rowIndex, columnIndex, row, actions }) => (
    <div
      className={`row-start-${rowIndex + 1} col-start-${columnIndex + 1} self-center justify-self-center`}
    >
      {
        actions.map(({ icon, onClickItem }) => {
          const Icon = icons[icon];
          return (
            <Icon
              className='cursor-pointer text-accent-100'
              key={`action-${rowIndex}`}
              onClick={onClickAction({ row, onClickItem })}
            />
          );
        })
      }
    </div>
  ),
  headerActions: ({ headerActions, columnsCount }) => (
    <div
      className={`row-start-1 col-start-1 col-end-${columnsCount} self-center justify-self-end mr-5 text-2xl`}
    >
      {
        headerActions.map(({ icon, onClickItem }) => {
          const Icon = icons[icon];
          return (
            <Icon
              className='cursor-pointer text-accent-100'
              key={`headerActions-${icon}`}
              onClick={onClickHeaderAction({ onClickItem })}
            />
          );
        })
      }
    </div>
  ),
};

Cell.header.propTypes = {
  columnIndex: propTypes.number.isRequired,
  label: propTypes.string.isRequired,
};

Cell.text.propTypes = {
  rowIndex: propTypes.number.isRequired,
  columnIndex: propTypes.number.isRequired,
  row: propTypes.string.isRequired,
  columnKey: propTypes.string.isRequired,
};

Cell.change.propTypes = {
  rowIndex: propTypes.number.isRequired,
  columnIndex: propTypes.number.isRequired,
  row: propTypes.string.isRequired,
  columnKey: propTypes.string.isRequired,
};

Cell.actions.propTypes = {
  rowIndex: propTypes.number.isRequired,
  columnIndex: propTypes.number.isRequired,
  row: propTypes.string.isRequired,
  actions: propTypes.arrayOf(propTypes.shape({
    icon: propTypes.string,
    onClickItem: propTypes.func,
  })).isRequired,
};

Cell.headerActions.propTypes = {
  headerActions: propTypes.arrayOf(propTypes.shape({
    icon: propTypes.string,
    onClickItem: propTypes.func,
  })).isRequired,
  columnsCount: propTypes.number.isRequired,
};

const TableCell = ({ type, ...others }) => {
  const renderCell = Cell[type];
  return renderCell({ ...others });
};

TableCell.propTypes = {
  type: propTypes.oneOf([ 'header', 'text', 'actions', 'headerActions' ]),
};

export default TableCell;