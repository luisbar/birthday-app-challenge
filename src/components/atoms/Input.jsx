import React, { useEffect, useState } from 'react';
import propTypes from 'prop-types';
import Text from '@atoms/Text';
import { useActor } from '@xstate/react';

const Input = ({ machine, className, input, text }) => {
  const [ state ] = useActor(machine);
  const [ error, setError ] = useState(undefined);

  useEffect(() => {
    if (state.matches('showingError')) setError(() => state.context.error);
    if (state.matches('cleaningError')) setError(() => undefined);
  }, [ state ]);

  return (
    <div
      className={`flex flex-col ${className}`}
    >
      {
        error
          ? (
            <Text
              label={error}
              className='text-red-300'
            />
          )
          : (
            <Text
              className={text.className}
              label={text.label}
              as={text.as}
            />
          )
      }
      <input
        disabled={state.matches('disabled')}
        className={`bg-gray-100 border-0 rounded-md disabled:cursor-not-allowed ${input.className}`}
        type={input.type}
        onChange={input.onChange}
        id={input.id}
      />
    </div>
  );
};

Input.propTypes = {
  machine: propTypes.object.isRequired,
  className: propTypes.string,
  input: propTypes.shape({
    type: propTypes.oneOf([ 'text', 'password', 'email', 'number', 'url', 'date', 'datetime-local', 'month', 'week', 'time', 'search', 'tel', 'checkbox', 'radio' ]).isRequired,
    className: propTypes.string,
    onChange: propTypes.func.isRequired,
    id: propTypes.string.isRequired,
  }).isRequired,
  text: propTypes.shape(Text.propTypes).isRequired,
};

Input.defaultProps = {
  className: '',
};

export default Input;