import React from 'react';
import propTypes from 'prop-types';
import Text from '@atoms/Text';
import { useActor } from '@xstate/react';

const Button = ({ machine, data, className, text }) => {
  const [ state, send ] = useActor(machine);

  const onClick = () => {
    send({
      data,
      type: 'PRESS',
    });
  };

  return (
    <button
      data-testid='button'
      type='button'
      className={`bg-accent-100 py-2 px-6 rounded-md uppercase active:shadow-inner disabled:cursor-not-allowed ${className}`}
      disabled={state.matches('disabled')}
      onClick={onClick}
    >
      <Text
        className={`text-light ${text.className}`}
        label={text.label}
        as={text.as}
      />
    </button>
  );
};

Button.propTypes = {
  machine: propTypes.object.isRequired,
  data: propTypes.object.isRequired,
  className: propTypes.string,
  text: propTypes.shape(Text.propTypes).isRequired,
};

Button.defaultProps = {
  className: '',
};

export default Button;