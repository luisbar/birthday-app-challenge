import React from 'react';
import Image from '@atoms/Image';
import Text from '@atoms/Text';
import propTypes from 'prop-types';

const Hero = ({ background, title, className }) => (
  <div
    className={`${className}`}
  >
    <Image
      src={background}
    />
    <Text
      as='h1'
      label={title}
      className='absolute top-0'
    />
  </div>
);

Hero.propTypes = {
  background: propTypes.string.isRequired,
  title: propTypes.string.isRequired,
  className: propTypes.string,
};

Hero.defaultProps = {
  className: '',
};

export default Hero;