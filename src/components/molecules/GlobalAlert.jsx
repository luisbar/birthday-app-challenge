import React, { createContext, useContext } from 'react';
import { useMachine } from '@xstate/react';
import machine from '@stateMachines/molecules/globalAlert';
import propTypes from 'prop-types';
import Text from '@atoms/Text';

const colors = {
  error: 'bg-error',
  success: 'bg-success',
};

const GlobalAlert = ({ message, type }) => (
  <div
    className={`h-24 w-full ${colors[type]} fixed flex justify-center items-center z-10`}
  >
    <Text
      as='h3'
      className='text-light-100'
      label={message}
    />
  </div>
);

GlobalAlert.propTypes = {
  message: propTypes.string.isRequired,
  type: propTypes.oneOf([ 'error', 'success' ]),
};

GlobalAlert.defaultProps = {
  type: 'error',
};

const GlobalAlertContext = createContext([]);
const GlobalAlertProvider = ({ children }) => {
  const [ state, send ] = useMachine(machine, { devTools: process.env.ENV === 'development' });

  return (
    <GlobalAlertContext.Provider
      value={[ state, send ]}
    >
      {
        state.matches('enabled') && (
        <GlobalAlert
          message={state.context.message}
          type={state.context.type}
        />
        )
      }
      {children}
    </GlobalAlertContext.Provider>
  );
};

GlobalAlertProvider.propTypes = {
  children: propTypes.node.isRequired,
};

export default GlobalAlertProvider;
export const useGlobalAlertContext = () => useContext(
  GlobalAlertContext,
);