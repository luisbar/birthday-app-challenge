import React from 'react';
import Text from '@atoms/Text';
import propTypes from 'prop-types';

const Card = ({ title, body, className }) => (
  <div
    className={`
    rounded-md shadow-md max-w-sm m-3 p-8 hover:shadow-xl dark:bg-dark-200 ${className}
    `}
  >
    <Text
      as='h4'
      label={title}
      className='flex flex-row justify-center my-5'
    />
    <Text
      as='md'
      label={body}
      className='m-2'
    />
  </div>
);

Card.propTypes = {
  title: propTypes.string.isRequired,
  body: propTypes.string.isRequired,
  className: propTypes.string,
};

Card.defaultProps = {
  className: '',
};

export default Card;