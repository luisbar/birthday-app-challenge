import React, { useEffect } from 'react';
import Main from '@templates/Main';
import { useGlobalSpinnerContext } from '@atoms/GlobalSpinner';
import { useGlobalAlertContext } from '@molecules/GlobalAlert';
import { useStoreContext } from '@root/stateMachines/Store';
import Table from '@organisms/Table';
import Paginator from '@molecules/Paginator';
import Input from '@atoms/Input';
import { useActor } from '@xstate/react';

const Home = () => {
  const { homeService } = useStoreContext();
  const [ homeState, send ] = useActor(homeService);
  const globalSpinnerContext = useGlobalSpinnerContext();
  const toggleSpinner = [ ...globalSpinnerContext ].pop();
  const globalAlertContext = useGlobalAlertContext();
  const toggleAlert = [ ...globalAlertContext ].pop();

  useEffect(() => {
    send({
      type: 'PROCESS',
      data: {
        pageNumber: homeState.context.pagination.pageNumber,
        pageSize: homeState.context.pagination.pageSize,
      },
    });
  }, []);

  useEffect(() => {
    if (homeState.matches('processing')) toggleSpinner({ type: 'ENABLE' });
    if (!homeState.matches('processing')) toggleSpinner({ type: 'DISABLE' });
    if (homeState.matches('failed')) toggleAlert({ type: 'ENABLE', data: { type: 'error', message: homeState.context.error.message } });
  }, [ homeState.value ]);

  return (
    <Main>
      <Input
        className='row-start-1 row-span-1 col-start-2 col-span-2'
        machine={homeState.children.rateNameInput}
        text={{
          label: 'home.txt5',
        }}
        input={{
          type: 'text',
          onChange: (e) => send({ type: 'FILTER', data: { filter: e.target.value } }),
        }}
      />
      <Table
        className='row-start-2 row-span-1 col-start-2 col-span-8'
        columns={[
          { key: 'rank', value: 'home.txt1', type: 'text' },
          { key: 'name', value: 'home.txt2', type: 'text' },
          { key: 'priceUsd', value: 'home.txt3', type: 'text' },
          { key: 'changePercent24Hr', value: 'home.txt4', type: 'change' },
        ]}
        rows={
          homeState.context.ratesFiltered.length
            ? homeState.context.ratesFiltered
            : homeState.context.rates
        }
      />
      <Paginator
        machine={homeState.children.paginator}
        className='row-start-3 row-span-1 col-start-6 col-span-4 h-14'
      />
    </Main>
  );
};

export default Home;
