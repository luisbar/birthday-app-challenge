import React, { useEffect } from 'react';
import Centered from '@templates/Centered';
import LoginForm from '@organisms/LoginForm';
import { useStoreContext } from '@stateMachines/Store';
import { useGlobalAlertContext } from '@molecules/GlobalAlert';
import { useGlobalSpinnerContext } from '@atoms/GlobalSpinner';
import { useNavigation } from 'react-navi';
import { useActor } from '@xstate/react';

const Login = () => {
  const { loginService } = useStoreContext();
  const [ loginFormState ] = useActor(loginService.children.get('loginForm'));
  const toggleSpinner = [ ...useGlobalSpinnerContext() ].pop();
  const toggleAlert = [ ...useGlobalAlertContext() ].pop();
  const navigation = useNavigation();

  useEffect(() => {
    if (loginFormState.matches('processing')) toggleSpinner({ type: 'ENABLE', data: { className: 'bg-light-100' } });
    if (!loginFormState.matches('processing')) toggleSpinner({ type: 'DISABLE' });
    if (loginFormState.matches('finished')) navigation.navigate('/');
    if (loginFormState.matches('failed')) toggleAlert({
      type: 'ENABLE',
      data: { type: 'error', message: loginFormState.context.error.message },
    });
  }, [ loginFormState.value ]);

  return (
    <Centered>
      <LoginForm />
    </Centered>
  );
};

export default Login;
