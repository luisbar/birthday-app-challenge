import joi from 'joi';

const getErrorMessages = ({ schema, data }) => {
  const { error } = schema.validate(data, { abortEarly: false });

  if (!error) return undefined;

  return error
    .details
    .map((detail) => `error.${detail.path[0]}.${detail.type}`);
};

const validateSessionData = (data = {}) => {
  const schema = joi.object({
    username: joi.string()
      .required()
      .email({ minDomainSegments: 2, tlds: { allow: [ 'com', 'net', 'io' ] } }),
    password: joi.string()
      .required()
      .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),
  });

  return getErrorMessages({ schema, data });
};

export default {
  validateSessionData,
};